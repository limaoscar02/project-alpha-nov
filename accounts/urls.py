from django.urls import path
from accounts.views import user_login, user_logout, signup

# from accounts.views import

# urlpatterns = [
#     path("",,name="home")
# ]

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
