from django.shortcuts import render
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


def my_tasks_list(request):
    tasks = Task.objects.all()
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/task_list.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/mine.html", context)
